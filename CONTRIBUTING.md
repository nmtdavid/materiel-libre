On essaye de suivre les recommandations d'utilisation participative de gitlab et d'Attlasian.

1.   on écrit jamais directement dans la branche master
2.   la branche de dev est la branche par défaut.
3.   on créé des branches uniquement sur la branche de dev
4.   pour créer une branche, une ouvre une issue avec un titre très parlant, qui invite à travailler sur un sujet, et on discute.
5.   depuis cette issue, on créé une branche qui porte le nom de l'issue, sur la branche de dev
6.   dans ces branches d'issue, on y place les fichiers que l'on veut, et on y discute les "commits".